// -- dependecies
//	bootstrap 3
// themify
// --

import cookie from 'js-cookie';

const cnpopup = {
	init(){
		window.cnpopup_config.img = JSON.parse(window.cnpopup_config.img);
		let banner_img = ( window.cnpopup_config && window.cnpopup_config.img && window.cnpopup_config.img.default ) ? window.cnpopup_config.img.default+'?id='+this.makeid(5) : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mN8Uw8AAl0Bbfn0s/kAAAAASUVORK5CYII=';
		let banner_w = ( window.cnpopup_config && window.cnpopup_config.width && window.cnpopup_config.width != '' ) ? parseInt(window.cnpopup_config.width)+'px' : 850+'px';
		let banner_h = ( window.cnpopup_config && window.cnpopup_config.height && window.cnpopup_config.height != '' && window.cnpopup_config.height != 'auto' ) ? parseInt(window.cnpopup_config.height)+'px' : 'auto';
		let banner_expiry = ( window.cnpopup_config && window.cnpopup_config.expiry && window.cnpopup_config.expiry != '' ) ? parseInt(window.cnpopup_config.expiry) : 1;
		let banner_html = ( window.cnpopup_config && window.cnpopup_config.html && window.cnpopup_config.html != '' ) ? window.cnpopup_config.html : '';

		if( window.innerWidth < 768 ){			
			if( window.cnpopup_config.img ){
				banner_img = window.cnpopup_config.img.xs+'?id='+this.makeid(5);
			}
		}else if( window.innerWidth > 768 && window.innerWidth < 992){
			if( window.cnpopup_config.img.sm ){
				banner_img = window.cnpopup_config.img.sm+'?id='+this.makeid(5);
			}
		}else if( window.innerWidth > 992 && window.innerWidth < 1200){
			if( window.cnpopup_config.img.md ){
				banner_img = window.cnpopup_config.img.md+'?id='+this.makeid(5);
			}
		}

		window.addEventListener('resize', function(){
			if( window.innerWidth < 768 ){			
				if( window.cnpopup_config.img ){
					document.querySelector('#cnpopup img').setAttribute('src',window.cnpopup_config.img.xs+'?id='+this.makeid(5));
				}
			}else if( window.innerWidth > 768 && window.innerWidth < 992){
				if( window.cnpopup_config.img.sm ){
					document.querySelector('#cnpopup img').setAttribute('src',window.cnpopup_config.img.sm+'?id='+this.makeid(5));
				}
			}else if( window.innerWidth > 992 && window.innerWidth < 1200){
				if( window.cnpopup_config.img.md ){
					document.querySelector('#cnpopup img').setAttribute('src',window.cnpopup_config.img.md+'?id='+this.makeid(5));
				}
			}			
		}, true);
		// create style
		let style = document.createElement('style');
		style.setAttribute('type',"text/css");
		style.setAttribute('rel',"stylesheet");
		style.innerHTML = `#cnpopup{width:${banner_w};height:${banner_h};}`;

		document.querySelector('head').appendChild(style);

		let el = document.createElement('div');
		el.classList.add('banner-click');
		el.setAttribute('id','cnpopup-wrapper');

		let closebtn = document.createElement('a');
			closebtn.href = '#';
			closebtn.setAttribute('id','cnpopup-close');
			closebtn.classList.add('cnpopup-exit');
			closebtn.innerHTML = '<i class="ti-close"></i>';

		el.innerHTML = `<div id="cnpopup">
						${banner_html}
						<img src="${banner_img}">
						</div>`;

		el.querySelector('#cnpopup').appendChild(closebtn);
		el.querySelectorAll('#cnpopup .cnpopup-exit').forEach( function(el){
			el.addEventListener('click', function() {
				event.preventDefault();

				if( window.cnpopup_config.domain != '' ){
					cookie.set(window.cnpopup_config.name,window.cnpopup_config.name,{ domain : 'connectnigeria.com', expires :  banner_expiry });
				}else{
					cookie.set(window.cnpopup_config.name,window.cnpopup_config.name,{ expires :  banner_expiry });
				}

				
				if( this.getAttribute('href') != '#' ){
					var redirectWindow = window.open(this.getAttribute('href'), '_blank');
				    redirectWindow.location;
				}
				
				if( document.querySelector('#cnpopup-wrapper') ){
					document.querySelector('#cnpopup-wrapper').remove();
				}
				
			});
		});
		document.querySelector('body').appendChild(el);
		setTimeout(function(){
			document.querySelector('#cnpopup-wrapper').style.display = 'flex';
		},parseInt(window.cnpopup_config.delay));
	},
	makeid(length){
		let result           = '';
		let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		let charactersLength = characters.length;
	   for ( var i = 0; i < length; i++ ) {
	      result += characters.charAt(Math.floor(Math.random() * charactersLength));
	   }
	   return result;
	}
}

if( !window.cnpopup_config.name ){
	console.log('popup name is required!');	
}else{
	var cnpopup_cookie = cookie.get(window.cnpopup_config.name);

	if( window.cnpopup_config.domain != '' ){	
		cnpopup_cookie = cookie.get(window.cnpopup_config.name,{ domain : window.cnpopup_config.domain != '' } );
	}
	if( !cnpopup_cookie ){
		cnpopup.init();
	}
}
